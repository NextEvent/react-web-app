import React, { Component, PropTypes } from 'react'
import ReactDOM, { findDOMNode } from 'react-dom'
import { Router, Route, Link, browserHistory, IndexRoute} from 'react-router'


import App from './containers/App.js'

import Home from './containers/Home/Home.js'
import Product from './containers/Product/Product.js'
import Category from './containers/Category/Category.js'

export default class Routes extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={App}>
          <IndexRoute component={Home} />
          <Route path="product" component={Product} />
          <Route path="category" component={Category} />
        </Route>
      </Router>
    )
  }
}