import React, { Component, PropTypes } from 'react'

import Carousel from '../../components/_Public/Carousel/Carousel.js'
import ProductList from '../../components/_Module/_ProductList/_productList.js'
import Section from '../../components/_Module/_Section/_section.js'
import Pclass from '../../components/_Module/_Pclass/_pclass.js'

export default class Home extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let slides = [
      {
        background: "http://localhost:8888/slider_1.png",
        link: "http://www.baidu.com/"
      },
      {
        background: "http://localhost:8888/slider_2.png",
        link: "http://www.caixie.net/"
      },
      {
        background: "http://localhost:8888/slider_3.png",
        link: "http://www.baidu.com/"
      },
      {
        background: "http://localhost:8888/slider_4.png",
        link: "http://www.baidu.com/"
      }
    ]
    let proLists = [
      {
        title: '优惠专场',
        img: 'http://localhost:8888/timg_1.jpeg',
        des: '［好奇纸尿裤］万千中国妈咪的第一选择',
        price: '160.00'
      },
      {
        title: '优惠专场',
        img: 'http://localhost:8888/timg_2.jpeg',
        des: '［好奇纸尿裤］万千中国妈咪的第一选择',
        price: '160.00'
      },
      {
        title: '优惠专场',
        img: 'http://localhost:8888/timg_3.jpeg',
        des: '［好奇纸尿裤］万千中国妈咪的第一选择',
        price: '160.00'
      },
      {
        title: '优惠专场',
        img: 'http://localhost:8888/timg_4.jpeg',
        des: '［好奇纸尿裤］万千中国妈咪的第一选择',
        price: '160.00'
      },
      {
        title: '优惠专场',
        img: 'http://localhost:8888/timg_5.jpeg',
        des: '［好奇纸尿裤］万千中国妈咪的第一选择',
        price: '160.00'
      }
    ]
    let sections = [
      {
        title: '孕妈妈',
        des: '孕妈妈必囤，24元起',
        img: 'http://localhost:8888/section1.png'
      },
      {
        title: '进口奶粉',
        des: '爱他美4.3折，牛栏5折',
        img: 'http://localhost:8888/section2.png'
      }
    ]
    let pclass = [
      {
        img: 'http://localhost:8888/class1.png'
      },
      {
        img: 'http://localhost:8888/class2.png'
      },
      {
        img: 'http://localhost:8888/class3.png'
      },
      {
        img: 'http://localhost:8888/class4.png'
      }
    ]
    return (
      <div>
        <Carousel data={slides} />
        <Section section={sections}/>
        <Pclass pclass={pclass}/>
        <ProductList proList={proLists} />
      </div>
    )
  }
}