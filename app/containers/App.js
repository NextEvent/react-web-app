import React, { Component, PropTypes } from 'react'
import {Link} from 'react-router'

import Header from '../components/header/header.js'
import ScrollUp from '../components/_Public/ScrollUp/ScrollUp.js'

import '../components/common.css'
import '../components/_Font/iconfont.css'

const ACTIVE = {color: 'red'}

export default class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let ul = 
        <ul>
          <li><Link to="/" activeStyle={ACTIVE}>home</Link></li>
          <li><Link to="/product" activeStyle={ACTIVE}>about</Link></li>
          <li><Link to="/category" activeStyle={ACTIVE}>category</Link></li>
        </ul>
    return (
      <div>
        <Header />
        <div className="has-header">
          {ul}
          {this.props.children}
        </div>
        <ScrollUp showUnder={200} />
      </div>
    )
  }
}