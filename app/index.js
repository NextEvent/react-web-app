import React, { Component, PropTypes } from 'react'
import ReactDOM, { findDOMNode } from 'react-dom'

import Routes from './routes.js'

// React.initializeTouchEvents(true)

ReactDOM.render(
  <Routes />,
  document.getElementById('root')
)