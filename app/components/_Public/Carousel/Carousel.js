import React, { Component, PropTypes } from 'react'
import Slider from 'react-slick'
import './Carousel.css'

export default class Carousel extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000
    }

    let styleImg = {
      width: '100%',
      height: '200px'
    }
    let itemDom = this.props.data.map((slider,index)=>{
      return (
        <div key={index}>
          <a href={slider.link}>
            <img style={styleImg} src={slider.background} />
          </a>
        </div>
      )
    })
    return (
      <div className="slider">
        <Slider {...settings}>
          {itemDom}
        </Slider>
      </div>
    )
  }
}