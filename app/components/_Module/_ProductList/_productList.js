import React, { Component, PropTypes } from 'react'
import './_productList.css'

export default class ProductList extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let proList = this.props.proList
    let views = proList.map((item,index)=>{
      return (
        <div key={index} className="p-list">
          <p className="p-list-sign">
            <i className="iconfont icon-pullup"></i>
            <span>{item.title}</span>
          </p>
          <div className="p-list-img">
            <img src={item.img} />
          </div>
          <h1 className="p-list-h1">{item.des}</h1>
          <p className="p-list-des">
            <span>¥</span>
            <span className="p-list-price">{item.price}</span>
            <span>元起</span>
            <span className="p-list-cut">1.7折</span>
          </p>
        </div>
      )
    })
    return (
      <div>
        {views}
      </div>
    )
  }
}