import React, { Component, PropTypes } from 'react'
import './_section.css'

export default class Section extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let sections = this.props.section
    let section = sections.map((item,index)=>{
      let style = {
        background: 'url("' + item.img + '") center center no-repeat',
        backgroundSize: '100% auto'
      }
      if (index%2 !== 0) {
        style = {
          borderLeft: '1px solid #ccc',
          marginLeft: '-1px',
          ...style
        }
        console.log(style)
      }
      return (
        <div key={index} className="sect" style={style}>
          <div className="sect-des">
            <div>{item.title}</div>
            <div>{item.des}</div>
          </div>
          <div className="sect-arr">
            >
          </div>
        </div>
      )
    })
    return (
      <div>
        {section}
      </div>
    )
  }
}