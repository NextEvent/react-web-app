import React, { Component, PropTypes } from 'react'
import './_pclass.css'

export default class Pclass extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let pclasses = this.props.pclass

    let style = {

    }
    let pclass = pclasses.map((item,index)=>{
      return (
        <div className="pclass" key={index}>
          <img src={item.img} />
        </div>
      )
    })
    return (
      <div>
        {pclass}
      </div>
    )
  }
}