import React, { Component, PropTypes } from 'react'
import { Router, Route, Link } from 'react-router'

import './slider.css'

import arrMiddle from './func.js'

export default class Slider extends Component {
  constructor(props) {
    super(props)

    this.state = {
      sliderIndex: 0,
      this:this
    }
    console.log("watch:",this.state)
  }
  changeTheMiddle(index) {
    console.log(index)
  }
  componentDidMount() {
    let sliderWidth = document.querySelector(".slider").offsetWidth
    let boxWidth = sliderWidth * 3
    console.log(boxWidth)
    let boxDom = document.querySelector('.slider-box')
    boxDom.style.width = boxWidth + "px"
    boxDom.style.left = -sliderWidth + "px"

    let sliderItem = document.querySelectorAll('.slider-box a')
    for (var i = 0; i < sliderItem.length; i++) {
      sliderItem[i].style.width = sliderWidth + 'px'
    }
  }
  render() {
    let slides = this.props.slides
    let viewSlideArr = arrMiddle(slides,this.state.sliderIndex)
    // console.log(this.state.sliderIndex,viewSlideArr)
    let leftSlider = slides.map((slide, index)=>{
      if (viewSlideArr[0]===index) {
        return (
          <Slide key={index}
            sliderlink={slide.sliderlink}
            background={slide.background} />
        )
      }
    })
    let midSlider = slides.map((slide, index)=>{
      if (viewSlideArr[1]===index) {
        return (
          <Slide key={index}
            index={index}
            sliderlink={slide.sliderlink}
            background={slide.background} />
        )
      }
    })
    let rightSlider = slides.map((slide, index)=>{
      if (viewSlideArr[2]===index) {
        return (
          <Slide key={index}
            sliderlink={slide.sliderlink}
            background={slide.background}
            changeTheMiddle={index=>{this.changeTheMiddle(index)}} />
        )
      }
    })
    let slide = [leftSlider,midSlider,rightSlider]
    return (
      <div className="slider">
        <div className="slider-box">
          {slide}
        </div>
      </div>
    )
  }
}

class Slide extends Component {
  constructor(props) {
    super(props)

    this.state = {
      startX: "",
      moveX: "",
      which:""
    }
  }
  touchStart(ev,which) {
    this.setState({startX:ev.touches[0].pageX,which:which})
    // console.log(this.state)
    // console.log(ev.touches[0])
  }
  touchEnd(ev,which) {
    let sliderItemDom = document.querySelector('.'+which)
    let sliderItemWidth = sliderItemDom.offsetWidth
    let sliderBoxDom = document.querySelector('.slider-box')
    // let offsetMove = Math.abs(this.state.moveX - this.state.startX) //绝对值
    let offsetMove = this.state.moveX - this.state.startX
    console.log(offsetMove,sliderItemWidth)
    if (offsetMove > 0 && (2 * offsetMove > sliderItemWidth)) {
      sliderBoxDom.style.marginLeft = sliderItemWidth + 'px'
    }else if(offsetMove < 0 && (Math.abs(2 * offsetMove) > sliderItemWidth)){
      sliderBoxDom.style.marginLeft = -sliderItemWidth + 'px'
    }else{
      sliderBoxDom.style.marginLeft = '0px'
    }
    this.setState({startX: "",moveX: ""})
    let middleIndex = parseFloat(which.substr(6))
    this.props.changeTheMiddle(middleIndex)
  }
  touchMove(ev,which) {
    this.setState({moveX:ev.touches[0].pageX})
    console.log(this.state)
    // console.log(ev.touches[0])
    // let sliderItemDom = document.querySelector('.'+which)
    let offsetMove = this.state.moveX - this.state.startX
    // sliderItemDom.style.marginLeft = offsetMove + 'px'
    let sliderBoxDom = document.querySelector('.slider-box')
    sliderBoxDom.style.marginLeft = offsetMove + 'px'
  }
  componentDidMount() {
    // document.querySelector('.slide').style.width = '400' + 'px'
  }
  render() {
    let sliderlink = this.props.sliderlink
    let background = this.props.background
    let index = "slider" + this.props.index
    let slideStyle = {
      backgroundImage: "url(" + background + ")"
    }
    return (
      <a className={index} href={sliderlink}>
        <div className="slide" style={slideStyle}
          onTouchStart={ev=>{this.touchStart(ev,index)}}
          onTouchEnd={ev=>{this.touchEnd(ev,index)}}
          onTouchMove={ev=>{this.touchMove(ev,index)}}>
        </div>
      </a>
    )
  }
}