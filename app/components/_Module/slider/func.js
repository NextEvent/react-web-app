// 找出数组某个坐标的前后坐标
export default function arrMiddle (arr, index) {
  var Arr = []
  if (arr[index+1] !== undefined) {
    if (arr[index-1] !== undefined){
      // return [arr[index-1],arr[index+1]]
      Arr = [index-1,index,index+1]
    }else{
      // return[arr[arr.length-1],arr[index+1]]
      Arr = [arr.length-1,index,index+1]
    }
  }else{
    if (arr[index-1] !== undefined) {
      // return [arr[index-1],arr[0]]
      Arr = [index-1,index,0]
    }else{
      // return [arr[arr.length-1],arr[0]]
      Arr = [arr.length-1,index,0]
    }
  }
  return Arr
}