import React, { Component, PropTypes } from 'react'
import { Router, Route, Link, Navigation} from 'react-router'

import './header.css'

export default class Header extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <header>
        <span><i className="iconfont icon-sort"></i></span>
        <h1>
          <img src="../../static/logo.png" />
        </h1>
        <span><i className="iconfont icon-my1"></i></span>
        <span><i className="iconfont icon-cart"></i></span>
      </header>
    )
  }
}